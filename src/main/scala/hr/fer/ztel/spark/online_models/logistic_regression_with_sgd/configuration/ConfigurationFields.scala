package hr.fer.ztel.spark.online_models.logistic_regression_with_sgd.configuration

object ConfigurationFields {

  /**
   * Key for accessing the location of configuration file
   */
  val CONFIG_FILE_DEFAULT_LOCATION: String = "config.properties"

  /**
   * Key for accessing the default Kafka group ID
   */
  val STREAM_KAFKA_GROUP_ID: String = "stream.kafka.groupId"

  /**
   * Key for accessing list of Kafka input stream brokers (host1:port1,host2:port2,...)
   */
  val STREAM_KAFKA_BROKERS: String = "stream.kafka.brokers"

  /**
   * Key for accessing the input stream topics
   */
  val STREAM_KAFKA_TOPICS_TEST_DATA: String = "stream.kafka.topics.testData"

  /**
   * Key for accessing the output stream topics
   */
  val STREAM_KAFKA_TOPIC_OUTPUT: String = "stream.kafka.topic.output"

  /**
   * Key for accessing the labeled data stream topics (for online learning)
   */
  val STREAM_KAFKA_TOPICS_TRAIN_DATA: String = "stream.kafka.topics.trainData"

  /**
   * Key for accessing Sparks master URL
   */
  val SPARK_MASTER_URL: String = "spark.master.url"

  /**
   * Key for accessing the default Spark application name
   */
  val SPARK_APP_NAME: String = "spark.appName"

  /**
   * Logging level
   */
  val LOGGING_LEVEL: String = "logging.level"

  /**
   * Spark streaming timeout duration
   */
  val CHECKPOINT_DIR: String = "spark.streaming.checkpoint"

  /**
   * Spark streaming batch time
   */
  val SPARK_STREAMING_BATCH_TIME_MILLIS: String = "spark.streaming.batchTime"

  /**
   * Spark streaming timeout duration
   */
  val SPARK_STREAMING_TIMEOUT_SECONDS: String = "spark.streaming.timeout"

  /**
   * Number of features in train/test dataset
   */
  val NUMBER_OF_FEATURES: String = "spark.ml.numOfFeatures"

  /**
   *  Number of iterations of gradient descent to run per update. Default: 50.
   */
  val NUMBER_OF_ITERATIONS: String = "spark.ml.model.numOfIterations"

  /**
   * Step size for gradient descent. Default: 0.1.
   */
  val STEP_SIZE: String = "spark.ml.model.stepSize"

  /**
   * Regularization parameter. Default: 0.0.
   */
  val REGULARIZATION_PARAMETER : String = "spark.ml.model.regularizationParameter"

  /**
   * Fraction of each batch to use for updates (used for controlling the concept drift). Default: 1.0.
   */
  val MICRO_BATCH_FRACTION: String = "spark.ml.model.microBatchFraction"
}
