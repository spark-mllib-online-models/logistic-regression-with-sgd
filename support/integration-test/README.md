# Integration test for Apache Spark ML classification models

Integration test for Apache Spark ML classification models capable for online learning using Kafka topic as train/test stream. It generates multiple linearly separable batches of numeric data, splits it into train/test set and sends it through two different topics. After all batches, it reads from the output topic and then performs the model evaluation and data visualization

---
## CLI

```
usage: olkit-c [-h] [-b [BOOTSTRAP_SERVERS [BOOTSTRAP_SERVERS ...]]]
               [-trt TRAIN_TOPIC] [-tet TEST_TOPIC] [-ot OUTPUT_TOPIC]
               [-o OUTPUT_FILE] [-ns NUM_SAMPLES] [-tt TRAIN_TEST]
               [-nf NUM_FEATURES] [-nif NUM_REDUNDANT] [-nit NUM_ITERATIONS]
               [-ai [ANOMALOUS_ITERATIONS [ANOMALOUS_ITERATIONS ...]]]
               [-cs CLASS_SEPARATION] [-ct CONSUMER_TIMEOUT_MS]
               [-r RANDOM_STATE] [-v VISUALIZE] [-sv SAVE_VISUALIZATIONS]

Integration test for Apache Spark ML classification models capable for online
learning using Kafka topic as train/test stream. It generates multiple
linearly separable batches of numeric data, splits it into train/test set and
sends it through two different topics. After all batches, it reads from the
output topic and then performs the model evaluation and data visualization.

optional arguments:
  -h, --help            show this help message and exit
  -b [BOOTSTRAP_SERVERS [BOOTSTRAP_SERVERS ...]], --bootstrap_servers [BOOTSTRAP_SERVERS [BOOTSTRAP_SERVERS ...]]
                        List of Kafka bootstrap servers.
  -trt TRAIN_TOPIC, --train_topic TRAIN_TOPIC
                        Kafka topic used as labeled (train) data stream.
                        (default: "train_data")
  -tet TEST_TOPIC, --test_topic TEST_TOPIC
                        Kafka topic used as test data stream. (default:
                        "test_data")
  -ot OUTPUT_TOPIC, --output_topic OUTPUT_TOPIC
                        Kafka topic from which the model prediction will be
                        read. (default: "output")
  -o OUTPUT_FILE, --output_file OUTPUT_FILE
                        Output file, default: stdout.
  -ns NUM_SAMPLES, --num_samples NUM_SAMPLES
                        Number of samples per batch which will later be split
                        into train and test set.
  -tt TRAIN_TEST, --train_test TRAIN_TEST
                        Train/test data ratio
  -nf NUM_FEATURES, --num_features NUM_FEATURES
                        Number of features which will be generated in the
                        sample. (default: 2)
  -nif NUM_REDUNDANT, --num_redundant NUM_REDUNDANT
                        Number of redundant features. These features are
                        generated as random linear combinations of the
                        informative features. (default: 0)
  -nit NUM_ITERATIONS, --num_iterations NUM_ITERATIONS
                        Number of iterations/batches after which the model
                        evaluation will be performed. (default: 10)
  -ai [ANOMALOUS_ITERATIONS [ANOMALOUS_ITERATIONS ...]], --anomalous_iterations [ANOMALOUS_ITERATIONS [ANOMALOUS_ITERATIONS ...]]
                        List of numbers which mark iteration as anomalous (to
                        mimic the concept drift).
  -cs CLASS_SEPARATION, --class_separation CLASS_SEPARATION
                        The factor multiplying the hypercube size. Larger
                        values spread out the clusters/classes and make the
                        classification task easier. (default: 1.0)
  -ct CONSUMER_TIMEOUT_MS, --consumer_timeout_ms CONSUMER_TIMEOUT_MS
                        Number of millis to wait after each record consumed
                        over output topic. (default: 15 sec)
  -r RANDOM_STATE, --random_state RANDOM_STATE
                        Random state
  -v VISUALIZE, --visualize VISUALIZE
                        Visualize datasets
  -sv SAVE_VISUALIZATIONS, --save_visualizations SAVE_VISUALIZATIONS
                        Save visualizations as png.
```